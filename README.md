# Fluent Text

Hi! To run Fluent Text on a Python IDE, you have to install some necessary libraries and understand how to add files and run the program. Please refer to this .md file to run the project.

Proudly made by: 
- Andre Lay - 2301874815 - andre.lay@binus.ac.id
- Gregorius Albert - 2301854486 - gregorius.albert@binus.ac.id
- Kevin Putra Yonathan - 2301871555 - kevin.yonathan@binus.ac.id

## Exporting The Project Files 
To run the program, it is recommended to run the program in a Python IDE like PyCharm (*recommended*). To run the program, please do the following:

1. Create a new project with a virtual environtment *(venv)*
2. Copy the Python source code (main.py) into the root directory of the project (eg: .../projectName/)
3. Copy the included audio and textfile. *(Optional)*

## Libraries

The libraries necessary are:

 1. numpy
 2. speechrecognition
 3. difflib
 4. termcolor
 5. pipwin
 6. pyaudio (install using pipwin)
 
 If you find a difficulty to get the necessary libraries, copy these commands into your venv terminal:
 
 **(Make sure pip is already installed)**

pip install numpy  
pip install speechrecognition  
pip install difflib  
pip install termcolor  
pip install pipwin  
pipwin install pyaudio

## Add audio and text files

To add your personal audio recording and text files, make sure the audio file is in .wav extension, and the text file is in .txt extension
Copy the file to the project root directory same with the main.py file
eg: ../projectName/

## Running the code

Run the main.py file and the program will prompt you to enter which audio file to be analyzed and the text file to be compared with.
For example you have an audio file named audio.wav and a text file named transcript.txt.
When the program prompted to enter the file name, just enter the file name without the extension, Example: 
- Enter audio file name []: audio
- Enter text file name []: transcript

