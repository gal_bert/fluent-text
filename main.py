import numpy as np
import speech_recognition as sr
from difflib import SequenceMatcher
import time
from termcolor import colored

# Please check the README.md file before running this program

# levenshetein function to find correction ratio
def levenshtein_ratio_and_distance(s, t, ratio_calc=False):
    rows = len(s) + 1
    cols = len(t) + 1
    distance = np.zeros((rows, cols), dtype=int)

    for i in range(1, rows):
        for k in range(1, cols):
            distance[i][0] = i
            distance[0][k] = k

    for col in range(1, cols):
        for row in range(1, rows):
            if s[row - 1] == t[col - 1]:
                cost = 0
            else:

                if ratio_calc == True:
                    cost = 2
                else:
                    cost = 1
            distance[row][col] = min(distance[row - 1][col] + 1,
                                     distance[row][col - 1] + 1,
                                     distance[row - 1][col - 1] + cost)
    if ratio_calc == True:
        Ratio = ((len(s) + len(t)) - distance[row][col]) / (len(s) + len(t))
        return Ratio
    else:
        return "The strings are {} edits away".format(distance[row][col])


# Similarity function check to get similarity percentage
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


rec = sr.Recognizer()

# Ask user to input Audio File Name .wav
print('Enter audio file name []: ', end="")
inputAudio = str(input()) + '.wav'

# Ask user to input Text File Name .txt
print('Enter text file name []: ', end="")
inputText = str(input()) + '.txt'

print()

# Read audio file
AudioSrc = sr.AudioFile(inputAudio)

# Read text file
textfile = open(inputText, 'rt').read().replace('\n', ' ')

# Print textfile (removing special chars to make comparison more fair)
print('Raw Text Data: ')
print(textfile)
textString = textfile.replace('.', '')
textString = textfile.replace(',', '')
textString = textfile.replace('!', '')
textString = textfile.replace('?', '')
textString = textfile.replace('\'', '')

print()
print("Processing Audio File...")
print()

# Process audio file
with AudioSrc as source:
    audio = rec.record(source)
try:
    analyzedAudio = rec.recognize_google(audio)
    print("Audio File:")
    print(analyzedAudio)
except Exception as e:
    print("Exception: " + str(e))

print()
print("Calculating", end="")
for i in range(3):
    time.sleep(0.5)
    print(".", end="")
print('')

# lowercase analyzed audio text
analyzedAudio = analyzedAudio.lower()

# lowercase text from textfile
textString = textString.lower()

print()

# Get similarity percentage
score = round(similar(analyzedAudio, textString) * 100)

# Print similarity
if score >= 85:
    print("Pronounciation Accuracy is: " + colored(str(score), 'green') + colored("%", 'green'))
elif score >= 65 and score < 85:
    print("Pronounciation Accuracy is: " + colored(str(score), 'yellow') + colored("%", 'yellow'))
else:
    print("Pronounciation Accuracy is: " + colored(str(score), 'red') + colored("%", 'red'))

# Print corrections amount
print(levenshtein_ratio_and_distance(analyzedAudio, textString))
